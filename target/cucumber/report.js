$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("MetabolicFactor.feature");
formatter.feature({
  "line": 1,
  "name": "Metabolic Factor Quiz",
  "description": "",
  "id": "metabolic-factor-quiz",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2046402600,
  "status": "passed"
});
formatter.scenario({
  "line": 2,
  "name": "Metabolic Factor Quiz",
  "description": "",
  "id": "metabolic-factor-quiz;metabolic-factor-quiz",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "User navigates to Metabolic Factor",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "Metabolic Factor quiz page displays",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "Factor: The first quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "Factor: User answers by selecting their age range",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Factor: The second quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "Factor: User answers by selecting their weight loss goal",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "Factor: The third quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "Factor: User answers by selecting their weight loss experience",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "Factor: The fourth quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "Factor: User answers by selecting their gender",
  "keyword": "Then "
});
formatter.step({
  "line": 19,
  "name": "Factor: The fifth quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Factor: User answers by selecting the place their weight tends to go",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "Factor: The sixth quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "Factor: User answers by selecting the food types they regularly consume",
  "keyword": "Then "
});
formatter.step({
  "line": 25,
  "name": "Factor: The seventh quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "Factor: User answers by selecting their level of motivation",
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "Factor: The eighth quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 29,
  "name": "Factor: User answers by selecting the answers that describe their average week",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "Factor: The page begins calculating the results",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "Factor: The Sign Up page is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "Factor: User inputs their email",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "Factor: User clicks the submit button",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "Factor: User is directed to the video page",
  "keyword": "Then "
});
formatter.step({
  "line": 38,
  "name": "Factor: User skips through the video to make the Add to Cart section display",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "Factor: User selects the first purchase option",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "Factor: User clicks the Add to Cart button",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "Factor: User is directed to the Payment Page",
  "keyword": "Then "
});
formatter.match({
  "location": "MetabolicFactor.user_navigates_to_Metabolic_Factor()"
});
formatter.result({
  "duration": 94105400,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.metabolic_Factor_quiz_page_displays()"
});
formatter.result({
  "duration": 1849783100,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_the_first_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 21187400,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_user_answers_by_selecting_their_age_range()"
});
formatter.result({
  "duration": 109227300,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_the_second_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 21506600,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_user_answers_by_selecting_their_weight_loss_goal()"
});
formatter.result({
  "duration": 107214200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_the_third_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 21977800,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_user_answers_by_selecting_their_weight_loss_experience()"
});
formatter.result({
  "duration": 93791200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_the_fourth_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 21225900,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_user_answers_by_selecting_their_gender()"
});
formatter.result({
  "duration": 95856800,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_the_fifth_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 21951300,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_user_answers_by_selecting_the_place_their_weight_tends_to_go()"
});
formatter.result({
  "duration": 164037200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_the_sixth_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 38217500,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_user_answers_by_selecting_the_food_types_they_regularly_consume()"
});
formatter.result({
  "duration": 541402200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_the_seventh_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 39997600,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_user_answers_by_selecting_their_level_of_motivation()"
});
formatter.result({
  "duration": 107380800,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_the_eighth_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 25924900,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_user_answers_by_selecting_the_answers_that_describe_their_average_week()"
});
formatter.result({
  "duration": 450494200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_the_page_begins_calculating_the_results()"
});
formatter.result({
  "duration": 3315540200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_the_Sign_Up_page_is_displayed()"
});
formatter.result({
  "duration": 1354321800,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_user_inputs_their_email()"
});
formatter.result({
  "duration": 116664500,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_user_clicks_the_submit_button()"
});
formatter.result({
  "duration": 101270500,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.f_user_is_directed_to_the_video_page()"
});
formatter.result({
  "duration": 3298779800,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.factor_User_skips_through_the_video_to_make_the_Add_to_Cart_section_display()"
});
formatter.result({
  "duration": 3799457700,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.factor_User_selects_the_first_purchase_option()"
});
formatter.result({
  "duration": 1169594000,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.factor_User_clicks_the_Add_to_Cart_button()"
});
formatter.result({
  "duration": 634801900,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicFactor.factor_User_is_directed_to_the_Payment_Page()"
});
formatter.result({
  "duration": 2780005700,
  "status": "passed"
});
formatter.after({
  "duration": 830031000,
  "status": "passed"
});
formatter.uri("MetabolicRenewal.feature");
formatter.feature({
  "line": 1,
  "name": "Metabolic Renewal Quiz",
  "description": "",
  "id": "metabolic-renewal-quiz",
  "keyword": "Feature"
});
formatter.before({
  "duration": 1306063300,
  "status": "passed"
});
formatter.scenario({
  "line": 2,
  "name": "Metabolic Renewal Quiz",
  "description": "",
  "id": "metabolic-renewal-quiz;metabolic-renewal-quiz",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "Renewal: User navigates to Metabolic Renewal",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "Renewal: Metabolic Renewal quiz page displays",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "Renewal: The first quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "Renewal: User answers by selecting their age range",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Renewal: The second quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "Renewal: User answers by selecting the answer that best describes their menstrual cycle",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "Renewal: The third quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "Renewal: User answers by selecting the answer that best describes their use of hormones",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "Renewal: The fourth quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "Renewal: User answers by selecting the answer that best describes their medical history",
  "keyword": "Then "
});
formatter.step({
  "line": 19,
  "name": "Renewal: The fifth quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Renewal: User answers by selecting the answer that best describes their body shape",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "Renewal: The sixth quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "Renewal: User answers by selecting the answer that best describes their most frustrating PMS symptom",
  "keyword": "Then "
});
formatter.step({
  "line": 25,
  "name": "Renewal: The seventh quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "Renewal: User answers by selecting the answer that best describes their most frustrating period symptom",
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "Renewal: The eighth quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 29,
  "name": "Renewal: User answers by selecting the main barrier preventing them from exercising",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "Renewal: The ninth quiz question is displayed",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "Renewal: User answers by selecting the biggest reason why dieting is a challenge",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "Renewal: The page begins calculating the results",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "Renewal: The Sign Up page is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 37,
  "name": "Renewal: User inputs their email",
  "keyword": "When "
});
formatter.step({
  "line": 38,
  "name": "Renewal: User clicks the submit button",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "Renewal: User is directed to the video page",
  "keyword": "Then "
});
formatter.step({
  "line": 41,
  "name": "Renewal: User skips through the video to make the Add to Cart section display",
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "Renewal: User clicks the Add to Cart button",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "Renewal: User is directed to the Payment Page",
  "keyword": "Then "
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_navigates_to_Metabolic_Renewal()"
});
formatter.result({
  "duration": 4379700,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_Metabolic_Renewal_quiz_page_displays()"
});
formatter.result({
  "duration": 2243130100,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_The_first_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 16675600,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_answers_by_selecting_their_age_range()"
});
formatter.result({
  "duration": 108575900,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_The_second_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 22659000,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_answers_by_selecting_the_answer_that_best_describes_their_menstrual_cycle()"
});
formatter.result({
  "duration": 622175200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_The_third_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 29486200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_answers_by_selecting_the_answer_that_best_describes_their_use_of_hormones()"
});
formatter.result({
  "duration": 114895800,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_The_fourth_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 23190200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_answers_by_selecting_the_answer_that_best_describes_their_medical_history()"
});
formatter.result({
  "duration": 108800300,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_The_fifth_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 22570300,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_answers_by_selecting_the_answer_that_best_describes_their_body_shape()"
});
formatter.result({
  "duration": 94223200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_The_sixth_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 23963900,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_answers_by_selecting_the_answer_that_best_describes_their_most_frustrating_PMS_symptom()"
});
formatter.result({
  "duration": 599341500,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_The_seventh_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 20368400,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_answers_by_selecting_the_answer_that_best_describes_their_most_frustrating_period_symptom()"
});
formatter.result({
  "duration": 148767000,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_The_eighth_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 26130200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_answers_by_selecting_the_main_barrier_preventing_them_from_exercising()"
});
formatter.result({
  "duration": 93502400,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_The_ninth_quiz_question_is_displayed()"
});
formatter.result({
  "duration": 21538900,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_answers_by_selecting_the_biggest_reason_why_dieting_is_a_challenge()"
});
formatter.result({
  "duration": 104634400,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_The_page_begins_calculating_the_results()"
});
formatter.result({
  "duration": 3407272000,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_The_Sign_Up_page_is_displayed()"
});
formatter.result({
  "duration": 6113923400,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_inputs_their_email()"
});
formatter.result({
  "duration": 84732500,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_clicks_the_submit_button()"
});
formatter.result({
  "duration": 1189202200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_is_directed_to_the_video_page()"
});
formatter.result({
  "duration": 5434061600,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_skips_through_the_video_to_make_the_Add_to_Cart_section_display()"
});
formatter.result({
  "duration": 415360200,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_clicks_the_Add_to_Cart_button()"
});
formatter.result({
  "duration": 5406325400,
  "status": "passed"
});
formatter.match({
  "location": "MetabolicRenewal.renewal_User_is_directed_to_the_Payment_Page()"
});
formatter.result({
  "duration": 2227830400,
  "status": "passed"
});
formatter.after({
  "duration": 835632800,
  "status": "passed"
});
});