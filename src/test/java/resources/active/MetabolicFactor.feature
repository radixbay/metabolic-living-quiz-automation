Feature: Metabolic Factor Quiz
Scenario: Metabolic Factor Quiz

	When User navigates to Metabolic Factor
	Then Metabolic Factor quiz page displays
	
	When Factor: The first quiz question is displayed 
	Then Factor: User answers by selecting their age range
	
	When Factor: The second quiz question is displayed
	Then Factor: User answers by selecting their weight loss goal
	
	When Factor: The third quiz question is displayed
	Then Factor: User answers by selecting their weight loss experience
	
	When Factor: The fourth quiz question is displayed
	Then Factor: User answers by selecting their gender 
	
	When Factor: The fifth quiz question is displayed
	Then Factor: User answers by selecting the place their weight tends to go
	
	When Factor: The sixth quiz question is displayed
	Then Factor: User answers by selecting the food types they regularly consume
	
	When Factor: The seventh quiz question is displayed
	Then Factor: User answers by selecting their level of motivation
	
	When Factor: The eighth quiz question is displayed
	Then Factor: User answers by selecting the answers that describe their average week
	
	When Factor: The page begins calculating the results 
	Then Factor: The Sign Up page is displayed
	
	When Factor: User inputs their email
	And Factor: User clicks the submit button
	Then Factor: User is directed to the video page
	
	When Factor: User skips through the video to make the Add to Cart section display
	And Factor: User selects the first purchase option
	And Factor: User clicks the Add to Cart button
	Then Factor: User is directed to the Payment Page