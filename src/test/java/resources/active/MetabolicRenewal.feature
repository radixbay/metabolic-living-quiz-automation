Feature: Metabolic Renewal Quiz
Scenario: Metabolic Renewal Quiz

	When Renewal: User navigates to Metabolic Renewal
	Then Renewal: Metabolic Renewal quiz page displays
	
	When Renewal: The first quiz question is displayed 
	Then Renewal: User answers by selecting their age range
	
	When Renewal: The second quiz question is displayed
	Then Renewal: User answers by selecting the answer that best describes their menstrual cycle
	
	When Renewal: The third quiz question is displayed
	Then Renewal: User answers by selecting the answer that best describes their use of hormones
	
	When Renewal: The fourth quiz question is displayed
	Then Renewal: User answers by selecting the answer that best describes their medical history
	
	When Renewal: The fifth quiz question is displayed
	Then Renewal: User answers by selecting the answer that best describes their body shape
	
	When Renewal: The sixth quiz question is displayed
	Then Renewal: User answers by selecting the answer that best describes their most frustrating PMS symptom
	
	When Renewal: The seventh quiz question is displayed
	Then Renewal: User answers by selecting the answer that best describes their most frustrating period symptom
	
	When Renewal: The eighth quiz question is displayed
	Then Renewal: User answers by selecting the main barrier preventing them from exercising
	
	When Renewal: The ninth quiz question is displayed
	Then Renewal: User answers by selecting the biggest reason why dieting is a challenge
	
	When Renewal: The page begins calculating the results 
	Then Renewal: The Sign Up page is displayed
	
	When Renewal: User inputs their email
	And Renewal: User clicks the submit button
	Then Renewal: User is directed to the video page
	
	When Renewal: User skips through the video to make the Add to Cart section display
	And Renewal: User clicks the Add to Cart button
	Then Renewal: User is directed to the Payment Page