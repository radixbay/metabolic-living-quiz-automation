package stepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;
import utils.ScoringTable;

public class MetabolicRenewal extends DriverFactory {
	
	public static int questionNumber = 1;
	String question;
	String answer;
	int submitInstance = 1;
	String hormoneType;
	
	public void passWhen() { System.out.print("--> [PASSED]\n");}
	public void passThen() { System.out.println("--> [PASSED]\n");}


	@When("^Renewal: User navigates to Metabolic Renewal$")
	public void renewal_User_navigates_to_Metabolic_Renewal() throws Throwable {
		rQuiz.navigateToMetabolicRenewal();   
	}

	@Then("^Renewal: Metabolic Renewal quiz page displays$")
	public void renewal_Metabolic_Renewal_quiz_page_displays() throws Throwable {
	    rQuiz.validateQuizPage();
	}

	@When("^Renewal: The first quiz question is displayed$")
	public void renewal_The_first_quiz_question_is_displayed() throws Throwable {
		question = rQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);   
	}

	@Then("^Renewal: User answers by selecting their age range$")
	public void renewal_User_answers_by_selecting_their_age_range() throws Throwable {
		answer = rQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;  
	}

	@When("^Renewal: The second quiz question is displayed$")
	public void renewal_The_second_quiz_question_is_displayed() throws Throwable {
		question = rQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);   
	}

	@Then("^Renewal: User answers by selecting the answer that best describes their menstrual cycle$")
	public void renewal_User_answers_by_selecting_the_answer_that_best_describes_their_menstrual_cycle() throws Throwable {
		answer = rQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;
	}

	@When("^Renewal: The third quiz question is displayed$")
	public void renewal_The_third_quiz_question_is_displayed() throws Throwable {
		question = rQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);   
	}

	@Then("^Renewal: User answers by selecting the answer that best describes their use of hormones$")
	public void renewal_User_answers_by_selecting_the_answer_that_best_describes_their_use_of_hormones() throws Throwable {
		answer = rQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;  
	}

	@When("^Renewal: The fourth quiz question is displayed$")
	public void renewal_The_fourth_quiz_question_is_displayed() throws Throwable {
		question = rQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);   
	}

	@Then("^Renewal: User answers by selecting the answer that best describes their medical history$")
	public void renewal_User_answers_by_selecting_the_answer_that_best_describes_their_medical_history() throws Throwable {
		answer = rQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;  
	}

	@When("^Renewal: The fifth quiz question is displayed$")
	public void renewal_The_fifth_quiz_question_is_displayed() throws Throwable {
		question = rQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);   
	}

	@Then("^Renewal: User answers by selecting the answer that best describes their body shape$")
	public void renewal_User_answers_by_selecting_the_answer_that_best_describes_their_body_shape() throws Throwable {
		answer = rQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;  
		ScoringTable.assembleScores();
		ScoringTable.determineType();
	}

	@When("^Renewal: The sixth quiz question is displayed$")
	public void renewal_The_sixth_quiz_question_is_displayed() throws Throwable {
		questionNumber = 11;
		question = rQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + (questionNumber - 5) + ". " + question);   
	}

	@Then("^Renewal: User answers by selecting the answer that best describes their most frustrating PMS symptom$")
	public void renewal_User_answers_by_selecting_the_answer_that_best_describes_their_most_frustrating_PMS_symptom() throws Throwable {
		answer = rQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;  
	}

	@When("^Renewal: The seventh quiz question is displayed$")
	public void renewal_The_seventh_quiz_question_is_displayed() throws Throwable {
		question = rQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + (questionNumber - 5) + ". " + question);   
	}

	@Then("^Renewal: User answers by selecting the answer that best describes their most frustrating period symptom$")
	public void renewal_User_answers_by_selecting_the_answer_that_best_describes_their_most_frustrating_period_symptom() throws Throwable {
		answer = rQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;  
	}

	@When("^Renewal: The eighth quiz question is displayed$")
	public void renewal_The_eighth_quiz_question_is_displayed() throws Throwable {
		question = rQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + (questionNumber - 5) + ". " + question);   
	}

	@Then("^Renewal: User answers by selecting the main barrier preventing them from exercising$")
	public void renewal_User_answers_by_selecting_the_main_barrier_preventing_them_from_exercising() throws Throwable {
		answer = rQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;  
	}

	@When("^Renewal: The ninth quiz question is displayed$")
	public void renewal_The_ninth_quiz_question_is_displayed() throws Throwable {
		question = rQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + (questionNumber - 5) + ". " + question);   
	}

	@Then("^Renewal: User answers by selecting the biggest reason why dieting is a challenge$")
	public void renewal_User_answers_by_selecting_the_biggest_reason_why_dieting_is_a_challenge() throws Throwable {
		answer = rQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;  
	}

	@When("^Renewal: The tenth quiz question is displayed$")
	public void renewal_The_tenth_quiz_question_is_displayed() throws Throwable {
		question = rQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + (questionNumber - 5) + ". " + question);   
	}

	@Then("^Renewal: User answers by selecting their most important issue they want resolved$")
	public void renewal_User_answers_by_selecting_their_most_important_issue_they_want_resolved() throws Throwable {
		answer = rQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;  
	}

	@When("^Renewal: The eleventh quiz question is displayed$")
	public void renewal_The_eleventh_quiz_question_is_displayed() throws Throwable {
		question = rQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + (questionNumber - 5) + ". " + question);   
	}

	@Then("^Renewal: User answers by selecting their weight loss time goal$")
	public void renewal_User_answers_by_selecting_their_weight_loss_time_goal() throws Throwable {
		answer = rQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;  
	}

	@When("^Renewal: The twelfth quiz question is displayed$")
	public void renewal_The_twelfth_quiz_question_is_displayed() throws Throwable {
		question = rQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + (questionNumber - 5) + ". " + question);   
	}

	@Then("^Renewal: User answers by selecting yes or no$")
	public void renewal_User_answers_by_selecting_yes_or_no() throws Throwable {
		answer = rQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;  
	}

	@When("^Renewal: The page begins calculating the results$")
	public void renewal_The_page_begins_calculating_the_results() throws Throwable {
		System.out.println("Validating Calculation Messages: ");
		rQuiz.validateCalculation(); 
	}

	@Then("^Renewal: The Sign Up page is displayed$")
	public void renewal_The_Sign_Up_page_is_displayed() throws Throwable {
		System.out.print("\nValidating Sign Up Page: \t");
		rSignUp.validateSignUpPage();
	    passThen();
	    rSignUp.validateHormoneTypePageResult();    
	}

	@When("^Renewal: User inputs their email$")
	public void renewal_User_inputs_their_email() throws Throwable {
		System.out.print("\nInputting Email: \t\t");
	    rSignUp.inputEmail();
	    passWhen();
	}

	@When("^Renewal: User clicks the submit button$")
	public void renewal_User_clicks_the_submit_button() throws Throwable {
		System.out.print("Clicking Submit: \t\t");
		rSignUp.clickSubmit(); 
		passWhen();
	}

	@Then("^Renewal: User is directed to the video page$")
	public void renewal_User_is_directed_to_the_video_page() throws Throwable {
		System.out.print("Validating Video Page: \t\t");
	    rVideo.validatePage();
	    passThen();
	}
	
	@When("^Renewal: User skips through the video to make the Add to Cart section display$")
	public void renewal_User_skips_through_the_video_to_make_the_Add_to_Cart_section_display() throws Throwable {
		System.out.print("Skipping Video: \t\t");
	    rVideo.scrollVideo();
	    passWhen();
	}

	@When("^Renewal: User clicks the Add to Cart button$")
	public void renewal_User_clicks_the_Add_to_Cart_button() throws Throwable {
		System.out.print("Adding to Cart: \t\t");
	    rVideo.clickAddToCart();
	    passWhen();
	}

	@Then("^Renewal: User is directed to the Payment Page$")
	public void renewal_User_is_directed_to_the_Payment_Page() throws Throwable {
		System.out.print("Clicking Submit: \t\t");
	    rPayment.clickPayPal();    
	    rPayment.validatePayPalSelected();
	    passThen();
	}

}
