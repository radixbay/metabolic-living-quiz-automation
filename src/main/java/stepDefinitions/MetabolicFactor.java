package stepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;

public class MetabolicFactor extends DriverFactory {

	int questionNumber = 1;
	String question;
	String answer;
	int submitInstance = 1;
	String fatBurningType;
	
	public void passWhen() { System.out.print("--> [PASSED]\n");}
	public void passThen() { System.out.println("--> [PASSED]\n");}

	@When("User navigates to Metabolic Factor$")
	public void user_navigates_to_Metabolic_Factor() throws Throwable {
		fQuiz.navigateToMetabolicFactor();
	}

	@Then("^Metabolic Factor quiz page displays$")
	public void metabolic_Factor_quiz_page_displays() throws Throwable {
		fQuiz.validateQuizPage();
	}

	@When("^Factor: The first quiz question is displayed$")
	public void f_the_first_quiz_question_is_displayed() throws Throwable {
		question = fQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);
	}

	@Then("^Factor: User answers by selecting their age range$")
	public void f_user_answers_by_selecting_their_age_range() throws Throwable {
		answer = fQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;
	}

	@When("^Factor: The second quiz question is displayed$")
	public void f_the_second_quiz_question_is_displayed() throws Throwable {
		question = fQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);
	}

	@Then("^Factor: User answers by selecting their weight loss goal$")
	public void f_user_answers_by_selecting_their_weight_loss_goal() throws Throwable {
		answer = fQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;
	}

	@When("^Factor: The third quiz question is displayed$")
	public void f_the_third_quiz_question_is_displayed() throws Throwable {
		question = fQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);
	}

	@Then("^Factor: User answers by selecting their weight loss experience$")
	public void f_user_answers_by_selecting_their_weight_loss_experience() throws Throwable {
		answer = fQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;
	}

	@When("^Factor: The fourth quiz question is displayed$")
	public void f_the_fourth_quiz_question_is_displayed() throws Throwable {
		question = fQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);
	}

	@Then("^Factor: User answers by selecting their gender$")
	public void f_user_answers_by_selecting_their_gender() throws Throwable {
		answer = fQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;
	}

	@When("^Factor: The fifth quiz question is displayed$")
	public void f_the_fifth_quiz_question_is_displayed() throws Throwable {
		question = fQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);
	}

	@Then("^Factor: User answers by selecting the place their weight tends to go$")
	public void f_user_answers_by_selecting_the_place_their_weight_tends_to_go() throws Throwable {
		answer = fQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;
	}

	@When("^Factor: The sixth quiz question is displayed$")
	public void f_the_sixth_quiz_question_is_displayed() throws Throwable {
		question = fQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);
	}

	@Then("^Factor: User answers by selecting the food types they regularly consume$")
	public void f_user_answers_by_selecting_the_food_types_they_regularly_consume() throws Throwable {
		answer = fQuiz.getAnswer(questionNumber);
		questionNumber++;
		fQuiz.clickSubmit(submitInstance);
		submitInstance++;
	}

	@When("^Factor: The seventh quiz question is displayed$")
	public void f_the_seventh_quiz_question_is_displayed() throws Throwable {
		question = fQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);
	}

	@Then("^Factor: User answers by selecting their level of motivation$")
	public void f_user_answers_by_selecting_their_level_of_motivation() throws Throwable {
		answer = fQuiz.getAnswer(questionNumber);
		System.out.println("Answer: " + answer + "\n");
		questionNumber++;
	}

	@When("^Factor: The eighth quiz question is displayed$")
	public void f_the_eighth_quiz_question_is_displayed() throws Throwable {
		question = fQuiz.getQuestion(questionNumber);
		System.out.println("Question #" + questionNumber + ". " + question);
	}

	@Then("^Factor: User answers by selecting the answers that describe their average week$")
	public void f_user_answers_by_selecting_the_answers_that_describe_their_average_week() throws Throwable {
		answer = fQuiz.getAnswer(questionNumber);
		questionNumber++;
		fQuiz.clickSubmit(submitInstance);
	}
	
	@When("^Factor: The page begins calculating the results$")
	public void f_the_page_begins_calculating_the_results() throws Throwable {
		System.out.println("Validating Calculation Messages: ");
		fQuiz.validateCalculation();
	}

	@Then("^Factor: The Sign Up page is displayed$")
	public void f_the_Sign_Up_page_is_displayed() throws Throwable {
		System.out.print("\nValidating Sign Up Page: \t");
		fSignUp.validatePage();
		passWhen();
	}
	
	@When("^Factor: User inputs their email$")
	public void f_user_inputs_their_email() throws Throwable {
		System.out.print("Inputting email: \t\t");
		fSignUp.inputEmail();
		passWhen();
	}

	@When("^Factor: User clicks the submit button$")
	public void f_user_clicks_the_submit_button() throws Throwable {
		fSignUp.clickSubmit();
		System.out.print("Clicking Submit Button: \t");
		passWhen();
	}

	@Then("^Factor: User is directed to the video page$")
	public void f_user_is_directed_to_the_video_page() throws Throwable {
		System.out.print("Validating Video Page: \t\t");
		fVideo.validatePage();
		passThen();
	}
	
	@When("^Factor: User skips through the video to make the Add to Cart section display$")
	public void factor_User_skips_through_the_video_to_make_the_Add_to_Cart_section_display() throws Throwable {
		System.out.print("Skipping Video: \t\t");
		fVideo.scrollVideo();
		fVideo.validateCountdown();
		passWhen();
	}

	@When("^Factor: User selects the first purchase option$")
	public void factor_User_selects_the_first_purchase_option() throws Throwable {
		System.out.print("Selecting Version: \t\t");
	    fVideo.selectVersion();
	    passWhen();

	}

	@When("^Factor: User clicks the Add to Cart button$")
	public void factor_User_clicks_the_Add_to_Cart_button() throws Throwable {
		System.out.print("Adding to Cart: \t\t");
	    fVideo.clickAddToCart();
	    passWhen();

	}

	@Then("^Factor: User is directed to the Payment Page$")
	public void factor_User_is_directed_to_the_Payment_Page() throws Throwable {
		System.out.print("Clicking Submit Button: \t");
	    fPayment.clickPayPal();
	    fPayment.validatePayPalSelected();
	    passThen();
	}
}
