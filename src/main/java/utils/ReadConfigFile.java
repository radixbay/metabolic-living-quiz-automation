package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadConfigFile extends DriverFactory {

	protected Properties prop = null;
	protected File src;
	protected FileInputStream fis;

	public ReadConfigFile() {
		try {
			src = new File(Constant.CONFIG_PROPERTIES_DIRECTORY);
			fis = new FileInputStream(src);
			prop = new Properties();		
			prop.load(fis);
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getBrowser() {
		return prop.getProperty("browser");
	}
	
	public String getState() {
		return prop.getProperty("headless");
	}
	
	public String getRBEmail() {
		return prop.getProperty("emailRB");
	}
	
	public String getKnowableEmail() {
		return prop.getProperty("emailKnowable");
	}
	
	public String getInboxPW() {
		return prop.getProperty("passwordInbox");
	}
	
	public String getKnowablePW() {
		return prop.getProperty("passwordKI");
	}
	
	public String getRelativityPW() {
		return prop.getProperty("passwordRelativity");
	}
	
	public String getFirstName() {
		return prop.getProperty("firstName");
	}
	
	public String getLastName() {
		return prop.getProperty("lastName");
	}
}