package utils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

public class Constant extends DriverFactory {
	
	/** Config Properties file **/
	public final static String CONFIG_PROPERTIES_DIRECTORY = Paths.get(System.getProperty("user.dir"), "config.properties").toString();
	
	/** Browser driver files **/	
	public final static String GECKO_DRIVER_DIRECTORY_MAC = Paths.get(System.getProperty("user.dir"), "drivers/Mac", "geckodriver").toString();	
	public final static String GECKO_DRIVER_DIRECTORY_WINDOWS = Paths.get(System.getProperty("user.dir"), "drivers\\Windows", "geckodriver.exe").toString();	
	public final static String CHROME_DRIVER_DIRECTORY_MAC = Paths.get(System.getProperty("user.dir"), "drivers/Mac", "chromedriver").toString();
	public final static String CHROME_DRIVER_DIRECTORY_WINDOWS = Paths.get(System.getProperty("user.dir"), "drivers\\Windows", "chromedriver.exe").toString();
	
	/** Downloads directory **/	
	public final static Path DOWNLOADS_DIRECTORY = Paths.get(System.getProperty("user.home"), "Downloads");

	/** Date Constants **/
	public final static LocalDate NOW = LocalDate.now();
	public final static String THE_DATE = NOW.toString().split("-")[0] + "-" + NOW.toString().split("-")[1] + "-" + NOW.toString().split("-")[2];  
	public final static String PAST_YEAR_START = Integer.toString(Integer.parseInt(Constant.THE_DATE.split("-")[0]) - 1) + "-01-01";

	/** URLs **/
	public final static String URL_EMAIL = "https://outlook.office.com/mail/inbox";
	public final static String URL_KCI = "https://stage.kci.theknowable.com/";
	public final static String URL_RELATIVITY = "https://transform.irisbyaxiom.com/Relativity";
	
	/** BrowserStack Integration **/
	public static final String USERNAME = System.getenv("BROWSERSTACK_USERNAME");
	public static final String AUTOMATE_KEY = System.getenv("BROWSERSTACK_ACCESS_KEY");
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
}