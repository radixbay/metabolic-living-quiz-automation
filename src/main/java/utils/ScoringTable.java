package utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pageObjects.renewal.RenewalQuiz;

public class ScoringTable extends RenewalQuiz {

	public ScoringTable() throws IOException {
		super();
	}

	public static int q1aID = 0, q2aID = 0, aID = 0, type1_score = 0, type2_score = 0, type3_score = 0, type4_score = 0,
			type5_score = 0, type6_score = 0, type7_score = 0, highestValue = 0, highestIndex = 0, typeTieBreaker = 0,
			typeResult = 0;
	public static boolean type4_trigger = false, type5_trigger = false, type6_trigger = false, type7_trigger = false;
	public static List<Integer> scoreSet = new ArrayList<Integer>(), indices = new ArrayList<Integer>(),
			triggerTieBreakers = new ArrayList<Integer>();
	public static List<Boolean> triggerSet = new ArrayList<Boolean>();
	public static String type = "";

	public static void getAnswerId(int aNum) {
		aID = aNum;
	}

	public static void increaseTotal(int qNum) {
		switch (qNum) {
		case 1: // Age
			switch (aID) {
			case 1: // 18 - 30
				type1_score += 2;
				break;
			case 2: // 31 - 44
				type1_score += 1;
				break;
			case 3: // 45 -54
				type5_score += 4;
				break;
			case 4: // 55 - 64
				type6_score += 5;
				break;
			case 5: // 65+
				type7_score += 10;
				break;
			default:
				break;
			}
			q1aID = aID; // Remember age answer to use as conditional for score tallies of future answers
			break;
		case 2: // Menstrual Cycle
			switch (aID) {
			case 1: // Easy, normal, regular
				type1_score += 2;
				typeTieBreaker = 0;
				break;
			case 2: // Regular, heavy flow, cramping
				type2_score += 4;
				typeTieBreaker = 1;
				break;
			case 3: // Regular, PMS until flow start
				type3_score += 4;
				typeTieBreaker = 2;
				break;
			case 4: // Irregular for 3+ months
				switch (q1aID) {
				case 1: // 18 - 30
				case 2: // 31 - 44
					type4_score += 4;
					typeTieBreaker = 3;
					break;
				case 3: // 45 -54
				case 4: // 55 - 64
				case 5: // 65+
					type5_score += +2;
					typeTieBreaker = 4;
					break;
				default:
					break;
				}
				break;
			case 5: // Not menstruated in 1+ year
			case 6: // Partial hysterectomy
				switch (q1aID) {
				case 1: // 18 - 30
				case 2: // 31 - 44
					type4_trigger = true;
					triggerTieBreakers.add(3);
					break;
				case 3: // 45 -54
				case 4: // 55 - 64
					type6_trigger = true;
					triggerTieBreakers.add(5);
					break;
				case 5: // 65+
					type7_trigger = true;
					triggerTieBreakers.add(6);
					break;
				default:
					break;
				}
				break;
			case 7: // Complete hysterectomy
				switch (q1aID) {
				case 4: // <= 64
					type6_trigger = true;
					triggerTieBreakers.add(5);
					break;
				case 5: // 65+
					type7_trigger = true;
					triggerTieBreakers.add(6);
					break;
				default:
					break;
				}
				break;
			case 8: // Regular due to hormones
			case 9: // No longer menstruate due to hormones
				switch (q1aID) {
				case 1: // 18 - 30
				case 2: // 31 - 44
					type1_score += 4;
					typeTieBreaker = 0;
					break;
				case 3: // 45 - 54
					type5_score += 2;
					typeTieBreaker = 4;
					break;
				case 4: // 55 - 64
					type6_score += 2;
					typeTieBreaker = 5;
					break;
				case 5: // 65+
					type7_score += 2;
					typeTieBreaker = 6;
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
			q2aID = aID; // Remember Menstrual Cycle answer to break potential ties
			break;
		case 3: // Use of Hormones
			switch (aID) {
			case 1: // Don't use hormones
			case 2: // Birth control to prevent pregnancy
				type1_score += 2;
				break;
			case 3: // Birth control to control menstrual symptoms
				type2_score += 2;
				type3_score += 2;
				break;
			case 4: // Progesterone to control symptoms
				switch (q1aID) {
				case 3: // 45 - 54
					type5_score += 2;
					break;
				case 4: // 55 - 64
					type6_score += 2;
					break;
				case 5: // 65+
					type7_score += 2;
					break;
				default:
					break;
				}
				break;
			case 5: // HRT
				switch (q1aID) {
				case 3: // 45 - 54
					type5_trigger = true;
					triggerTieBreakers.add(4);
					break;
				case 4: // 55 - 64
					type6_trigger = true;
					triggerTieBreakers.add(5);
					break;
				case 5: // 65+
					type7_trigger = true;
					triggerTieBreakers.add(6);
					break;
				default:
					break;
				}
				break;
			}
			break;
		// Medical History
		case 4:
			switch (aID) {
			case 1: // Light - No issues
				type1_score += 2;
				break;
			case 2: // Severe cramps, heavy flow, polyps, endometriosis
				type2_score += 5;
				break;
			case 3: // Severe PMS, PCOS, relief from progesterone therapy
				type3_score += 4;
				break;
			case 4: // Both 2 + 3
				type4_score += 5;
				break;
			case 5: // Menopause
			case 6: // Partial hysterectomy
				switch (q1aID) {
				case 2: // 31 - 44
				case 3: // 45 - 54
					type5_trigger = true;
					triggerTieBreakers.add(4);
					break;
				case 4: // 55 - 64
					type6_trigger = true;
					triggerTieBreakers.add(5);
					break;
				case 5: // 65+
					type7_trigger = true;
					triggerTieBreakers.add(6);
					break;
				default:
					break;
				}
				break;
			case 7: // Complete hysterectomy
				switch (q1aID) {
				case 4: // <= 64
					type6_trigger = true;
					triggerTieBreakers.add(5);
					break;
				case 5: // 65+
					type7_trigger = true;
					triggerTieBreakers.add(6);
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
			break;
		// Body Shape and Age
		case 5:
			switch (aID) {
			case 1: // Normal
				type1_score += 2;
				break;
			case 2: // Curvy
				type2_score += 2;
				break;
			case 3: // Apple-shaped
				type3_score += 2;
				type4_score += 2;
				break;
			case 4: // Lost female shape since 45
				type5_trigger = true;
				triggerTieBreakers.add(4);
				break;
			case 5: // Gained belly fat since 55
				switch (q1aID) {
				case 4:// <= 64
					type6_trigger = true;
					triggerTieBreakers.add(5);
					break;
				case 5: // 65+
					type7_trigger = true;
					triggerTieBreakers.add(6);
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
	}

	public static void assembleScores() {
		Collections.addAll(scoreSet, type1_score, type2_score, type3_score, type4_score, type5_score, type6_score,
				type7_score);
		highestValue = Collections.max(scoreSet);
		for (int i = 0; i < scoreSet.size(); i++) {
			if (scoreSet.get(i) == highestValue)
				indices.add(i);
		}
	}

	public static void determineType() {
		Collections.addAll(triggerSet, type4_trigger, type5_trigger, type6_trigger, type7_trigger);
		if (triggerSet.contains(true))
			typeResult = triggerTieBreakers.get(0);
		else {
			if (indices.size() > 1)
				typeResult = typeTieBreaker;
			else
				typeResult = (indices.get(0));
		}
		typeResult += 1;
		switch (typeResult) {
		case 1:
			type = "Mixed Signal Metabolism (Normal Cycle – Balanced Estrogen and Progesterone)";
			break;
		case 2:
			type = "Hormone Overload (Normal Cycle – Estrogen Dominant)";
			break;
		case 3:
			type = "Hormone Shortfall (Normal Cycle – Progesterone Deficient)";
			break;
		case 4:
			type = "Ovarian Burnout (Normal Cycle – Estrogen Deficient & Progesterone Deficient)";
			break;
		case 5:
			type = "Metabolic Sputter (Perimenopause)";
			break;
		case 6:
			type = "Ovarian Fatigue (Menopause)";
			break;
		case 7:
			type = "Ovarian Shutdown (Postmenopause)";
			break;
		}
	}
}