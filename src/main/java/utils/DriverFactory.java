package utils;

import java.awt.Toolkit;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import pageObjects.BasePage;
import pageObjects.factor.FactorPayment;
import pageObjects.factor.FactorQuiz;
import pageObjects.factor.FactorSignUp;
import pageObjects.factor.FactorVideo;
import pageObjects.renewal.RenewalPayment;
import pageObjects.renewal.RenewalQuiz;
import pageObjects.renewal.RenewalSignUp;
import pageObjects.renewal.RenewalVideo;

@SuppressWarnings("unused")
public class DriverFactory {

	// INITIALIZE
	
	// ** Driver and Wait ** //
	public static WebDriver driver;
	public static WebDriverWait wait;
	public static SoftAssert soft = new SoftAssert();
	public static ArrayList<String> tabs;

	// ** Page Builders ** //
	public static FactorQuiz fQuiz;
	public static FactorSignUp fSignUp;
	public static FactorVideo fVideo;
	public static FactorPayment fPayment;
	public static RenewalQuiz rQuiz;
	public static RenewalSignUp rSignUp;
	public static RenewalVideo rVideo;
	public static RenewalPayment rPayment;
	
	// ** Miscellaneous Objects ** //
	public static BasePage basePage;
	public static Object browserState;
	public static String browserName;
	
	// ** Miscellaneous Variables ** //
	protected String os = System.getProperty("os.name");
	
	public WebDriver getDriver() {

		// Read Config
		try {
			ReadConfigFile file = new ReadConfigFile();
			browserName = file.getBrowser();
			browserState = file.getState();			
			String chromeDriver, firefoxDriver;
			if (os.contains("Windows")) {
				chromeDriver = Constant.CHROME_DRIVER_DIRECTORY_WINDOWS;
				firefoxDriver = Constant.GECKO_DRIVER_DIRECTORY_WINDOWS;
			} else {
				chromeDriver = Constant.CHROME_DRIVER_DIRECTORY_MAC;
				firefoxDriver = Constant.GECKO_DRIVER_DIRECTORY_MAC;
			}
			switch (browserName) {
			case "firefox":
				if (null == driver) {
					System.setProperty("webdriver.gecko.driver", firefoxDriver);
					FirefoxOptions options = new FirefoxOptions();
					options.setCapability("marionette", false);
					driver = new FirefoxDriver();
					driver.manage().window().maximize();
				}
				break;
			case "chrome":
				if (null == driver) {
					System.setProperty("webdriver.chrome.driver", chromeDriver);
					ChromeOptions options = new ChromeOptions();
					HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
					chromePrefs.put("profile.default_content_settings.popups", 0);
					chromePrefs.put("download.default_directory", Constant.DOWNLOADS_DIRECTORY.toString());
					chromePrefs.put("browser.setDownloadBehavior", "allow");
					options.setExperimentalOption("prefs", chromePrefs);  
					// ChromeDriver is just AWFUL because every version or two it breaks unless you pass cryptic arguments
					options.setPageLoadStrategy(PageLoadStrategy.NONE); // https://www.skptricks.com/2018/08/timed-out-receiving-message-from-renderer-selenium.html
					options.addArguments("--enable-automation"); // https://stackoverflow.com/a/43840128/1689770
					options.addArguments("--no-sandbox"); //https://stackoverflow.com/a/50725918/1689770
					options.addArguments("--disable-infobars"); //https://stackoverflow.com/a/43840128/1689770
					options.addArguments("--disable-dev-shm-usage"); //https://stackoverflow.com/a/50725918/1689770
					options.addArguments("--disable-browser-side-navigation"); //https://stackoverflow.com/a/49123152/1689770
                    options.addArguments("--disable-extensions"); //to disable browser extension popup
					options.addArguments("--start-maximized"); // https://stackoverflow.com/a/26283818/1689770
					
					driver = new ChromeDriver(options);
				}
				break;
			case "safari":
				if (null == driver) {
					SafariOptions options = new SafariOptions();
					driver = new SafariDriver();
					driver.manage().window().maximize();
				}
				break;
			}
		} catch (Exception e) {
			System.out.println("Unable to load browser: " + e.getMessage());
		} finally {
			wait = new WebDriverWait(driver, 10);
			
			fQuiz = PageFactory.initElements(driver, FactorQuiz.class);
			fSignUp = PageFactory.initElements(driver, FactorSignUp.class);
			fVideo = PageFactory.initElements(driver, FactorVideo.class);
			fPayment = PageFactory.initElements(driver, FactorPayment.class);
			rQuiz = PageFactory.initElements(driver, RenewalQuiz.class);
			rSignUp = PageFactory.initElements(driver, RenewalSignUp.class);
			rVideo = PageFactory.initElements(driver, RenewalVideo.class);
			rPayment = PageFactory.initElements(driver, RenewalPayment.class);

			basePage = PageFactory.initElements(driver, BasePage.class);
		}
		return driver;
	}
}