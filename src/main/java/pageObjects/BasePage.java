package pageObjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isOneOf;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;

import junit.framework.ComparisonFailure;
import utils.Constant;
import utils.DriverFactory;

public class BasePage extends DriverFactory {
	protected WebDriverWait wait;
	protected FluentWait<WebDriver> tempWait;
	protected JavascriptExecutor jsExecutor;
	private static String screenshotName;
	private static String FAIL = "====> <[FAIL]>";
	

	public BasePage() throws IOException {
		wait = new WebDriverWait(driver, 10);
		jsExecutor = ((JavascriptExecutor) driver);
	}

	/**********************************************************************************
	 ** CLICK METHOD
	 **********************************************************************************/

	protected void waitAndClickElement(WebElement element) throws InterruptedException {
		try {
			waitForJSandJQueryToLoad();
			waitUntilElementIsClickable(element);
			element.click();
		} catch (StaleElementReferenceException e) {
			WebElement elementToClick = element;
			waitUntilElementIsClickable(elementToClick);
		} catch (ElementClickInterceptedException e) {
			waitAndclickElementUsingJS(element);
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------" 
					+ "\nElement: " + element.toString() 
					+ "\nReason: Element was not clicked." 
					+ "\nException: " + e.getClass().toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("FAIL: Unable to wait and click on the WebElement, Exception: " + e.getMessage());
		}
	}

	/**********************************************************************************
	 ** ACTION METHODS
	 **********************************************************************************/

	protected void actionHover(WebElement element) throws Exception {
		waitForJSandJQueryToLoad();
		waitUntilElementIsClickable(element);
		Actions action = new Actions(driver);
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			action.moveToElement(element).build().perform();
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = element;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(elementToClick)).isEnabled();
			if (elementPresent == true) {
				action.moveToElement(elementToClick).build().perform();
			}
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Unable to Action Move to the WebElement, using locator: " + "<" + element.toString() + ">" 
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage() 
					+ "\n------------------------------------------");
			Assert.fail("Unable to Action Move to the WebElement, Exception: " + e.getMessage());
		}
	}

	protected void actionClickAndDrag(WebElement from, WebElement to) throws Exception {
		waitForJSandJQueryToLoad();
		Actions action = new Actions(driver);
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(from)).isEnabled();
			action.dragAndDrop(from, to).build().perform();
		} catch (StaleElementReferenceException elementUpdate) {
			WebElement newFrom = from;
			WebElement newTo = to;
			Boolean fromPresent = wait.until(ExpectedConditions.elementToBeClickable(newFrom)).isEnabled();
			Boolean toPresent = wait.until(ExpectedConditions.elementToBeClickable(newTo)).isEnabled();
			if (fromPresent == true && toPresent == true ) {
				action.dragAndDrop(newFrom, to).build().perform();
			}
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Unable to Action Click and Drag on the WebElement, using locator: " + "<" + from.toString() + ">" 
					+ "\nException: " + (e.getClass()).toString().replace("class ", "")
					+ "\nMessage: " + e.getMessage() 
					+ "\n------------------------------------------");
			Assert.fail("Unable to Action Click and Drag on the WebElement, Exception: " + e.getMessage());
		}
	}
	
	protected void actionHoldKeyAndClick(WebElement[] element, Keys key) throws Exception {
		Actions action = new Actions(driver);
		try {
			action.keyDown(key)
			.click(element[0]).click(element[1]).click(element[2])
			.keyUp(key).build().perform();
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Unable to Action hold key \"" + key + "\" and click on the WebElement, using locator: " + "<" + element.toString() + ">" 
					+ "\nException: " + (e.getClass()).toString().replace("class ", "")
					+ "\nMessage: " + e.getMessage() 
					+ "\n------------------------------------------");
			Assert.fail("Unable to Action Hold Key and Click on the WebElement, Exception: " + e.getMessage());
		}
	}
	
	public void actionMoveAndClick(WebElement element) throws Exception {
		waitForJSandJQueryToLoad();
		waitUntilElementIsClickable(element);
		Actions action = new Actions(driver);
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			action.moveToElement(element).click().build().perform();
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = element;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(elementToClick)).isEnabled();
			if (elementPresent == true) {
				action.moveToElement(elementToClick).click().build().perform();
			}
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
				+ "Unable to Action Move and Click on the WebElement, using locator: " + "<" + element.toString() + ">"
				+ "\nException: " + (e.getClass()).toString().replace("class ", "")
				+ "\nMessage: " + e.getMessage() 
				+ "\n------------------------------------------");
			Assert.fail("Unable to Action Move and Click on the WebElement, Exception: " + e.getMessage());
		}
	}
	
	public void actionDoubleClick(WebElement element) throws Exception {
		Actions action = new Actions(driver);
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			action.doubleClick(element).build().perform();
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = element;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(elementToClick)).isEnabled();
			if (elementPresent == true) {
				action.doubleClick(elementToClick).build().perform();
			}
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "Unable to Action Double Click on the WebElement, using locator: " + "<" + element.toString() + ">"
					+ "\nException: " + (e.getClass()).toString().replace("class ", "")
					+ "\nMessage: " + e.getMessage() 
					+ "\n------------------------------------------");
			Assert.fail("Unable to Action Move and Click on the WebElement, Exception: " + e.getMessage());
		}
	}
	
	public void actionClickAndHold(WebElement element) throws Exception {
		Actions action = new Actions(driver);
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			action.clickAndHold(element).build().perform();
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = element;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(elementToClick)).isEnabled();
			if (elementPresent == true) {
				action.clickAndHold(elementToClick).build().perform();
			}
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "Unable to Action Click and Hold on the WebElement, using locator: " + "<" + element.toString() + ">"
					+ "\nException: " + (e.getClass()).toString().replace("class ", "")
					+ "\nMessage: " + e.getMessage() 
					+ "\n------------------------------------------");
			Assert.fail("Unable to Action Move and Click on the WebElement, Exception: " + e.getMessage());
		}
	}
	
	public void actionReleaseAfterClickAndHold(WebElement element) throws Exception {
		Actions action = new Actions(driver);
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			action.release().build().perform();
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = element;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(elementToClick)).isEnabled();
			if (elementPresent == true) {
				action.release().build().perform();
			}
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "Unable to Action Release on the WebElement, using locator: " + "<" + element.toString() + ">"
					+ "\nException: " + (e.getClass()).toString().replace("class ", "")
					+ "\nMessage: " + e.getMessage() 
					+ "\n------------------------------------------");
			Assert.fail("Unable to Action Move and Click on the WebElement, Exception: " + e.getMessage());
		}
	}


	/**********************************************************************************
	 ** SEND KEYS METHOD
	 **********************************************************************************/

	protected void sendKeysToWebElement(WebElement element, String textToSend) throws Exception {
		waitUntilElementIsVisible(element);
		try {
			if (!element.getAttribute("value").equals("")) {
				int characters = element.getAttribute("value").length();
				for (int i = 0; i < characters; i++) {
					sendKeysToWebElement(element, Keys.BACK_SPACE);
				}
			}
			element.sendKeys(textToSend);
			while (!element.getAttribute("value").equals(textToSend)) {
				element.clear();
				sendKeysToWebElement(element, textToSend);
			}
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Unable to send keys to WebElement: " + "<" + element.toString() + ">" 
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Unable to send keys to WebElement, Exception: " + e.getMessage());
		}
	}
	
	protected void sendKeysToWebElement(WebElement element, Keys key) throws Exception {
		waitForJSandJQueryToLoad();
		waitUntilElementIsClickable(element);
		waitUntilElementIsVisible(element);
		try {
			element.sendKeys(key);		
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Unable to send keys to WebElement: " + "<" + element.toString() + ">" 
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Unable to send keys to WebElement, Exception: " + e.getMessage());
		}
	}

	/**********************************************************************************
	 ** ASSERTION TESTS
	 **********************************************************************************/

	protected void softFail(String message, String stacktrace) throws Exception {
		soft.fail("[SOFT FAILURE]\n\tat " + stacktrace);
		System.out.print(message);
	}
	
	protected boolean assertTextExists(WebElement element) throws Exception {
		waitUntilElementIsVisible(element);
		try {
			if (!element.getAttribute("value").equals("")) return true;
		} catch (NullPointerException e) {
			if (!element.getText().equals("")) return true;
			else {
				System.out.println(FAIL 
						+ "\n------------------------------------------"
						+ "\nReason: Element input not populated." 
						+ "\nElement: <" + element.toString() + ">"
						+ "\nDiscovered input: " + element.getText()
						+ "\nDiscovered element class: " + element.getAttribute("class")
						+ "\nDiscovered element value: " + element.getAttribute("value")
						+ "\n------------------------------------------");
				Assert.fail("Input not found at element: " + element.toString());
			}	
		}
		return false;
	}
	
	protected boolean assertNumbersMatch(int expectedNum, int discoveredNum) throws Exception, ComparisonFailure {
		try { 
			Assert.assertEquals(expectedNum, discoveredNum);
			return true;
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Expected number not found." 
					+ "\nExpected text: \"" + expectedNum
					+ "\nDiscovered text: \"" + discoveredNum
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Expected text not found in WebElement, Exception: " + e.getMessage());
			return false;
		}
	}
	
	protected boolean assertTextMatches(String expectedText, WebElement element) throws Exception, ComparisonFailure {
		waitUntilElementIsVisible(element);
		expectedText = expectedText.toLowerCase().replaceAll("[^a-z0-9]|\\s", "");
		String discoveredText = element.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", "");
		try { 
			Assert.assertEquals(expectedText, discoveredText);
			return true;
		} catch (AssertionError e) {
			if (discoveredText.contains("failedtoresetpassword")) {			
				return false;
			}
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Expected text not found in element." 
					+ "\nExpected text: \"" + expectedText
					+ "\nDiscovered text: \"" + discoveredText
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Expected text not found in WebElement, Exception: " + e.getMessage());
			return false;
		}
	}
	
	
	
	protected boolean assertTextMatches(String expectedText, String discoveredText) throws Exception, ComparisonFailure {
		
		try { 
			Assert.assertEquals(expectedText.toLowerCase().replaceAll("[^a-z0-9]|\\s", ""), discoveredText.toLowerCase().replaceAll("[^a-z0-9]|\\s", ""));
			return true;
		} catch (AssertionError e) {
		
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Expected text not found." 
					+ "\nExpected text: \"" + expectedText + "\"; Converted to \"" + expectedText.toLowerCase().replaceAll("[^a-z0-9]|\\s", "") + "\""
					+ "\nDiscovered text: \"" + discoveredText + "\"; Converted to \"" + discoveredText.toLowerCase().replaceAll("[^a-z0-9]|\\s", "") + "\""
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Expected text not found in WebElement, Exception: " + e.getMessage());
			return false;
		}
	}
	
	protected boolean assertListsMatch(List<String> list1, List<String> list2) throws Exception, ComparisonFailure {
		Collections.sort(list1);
		Collections.sort(list2);
		try { 
			Assert.assertTrue(list1.equals(list2));
			return true;
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Expected text not found in element." 
					+ "\nExpected text: \"" + Arrays.toString(list1.toArray()) 
					+ "\nDiscovered text: \"" + Arrays.toString(list2.toArray())
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\n------------------------------------------");
			Assert.fail("Expected text not found in WebElement, Exception: " + e);
			return false;
		}
	}
	
	protected boolean assertDatesWithinPresetRange(List<LocalDate> allDates, int range) throws Exception, ComparisonFailure {
		LocalDate today = LocalDate.now();
		LocalDate ago = today.minusDays(range);
		LocalDate badDate = null;
		try { 
			for (LocalDate date: allDates) {
				if (date.isBefore(ago) || date.isAfter(today)) {
					badDate = date;
					Assert.fail();
				}
			}
			return true;
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Detected date falls outside of given range." 
					+ "\nStarting Date: " + ago 
					+ "\nEnding Date: " + today
					+ "\nDiscoverd Date: " + badDate
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\n------------------------------------------");
			Assert.fail("Detected date falls outside of given range, Exception: " + e);
			return false;
		}
	}
	
	protected boolean assertDatesWithinCustomRange(List<LocalDate> allDates, String theDate) throws Exception, ComparisonFailure {
		LocalDate today = LocalDate.now();
//		DateFormat originalFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
//		DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
//		Date ogSetDate = originalFormat.parse(theDate);
		LocalDate setDate = LocalDate.parse(theDate);
		LocalDate badDate = null;
		try { 
			for (LocalDate date: allDates) {
				if (date.isBefore(setDate) || date.isAfter(today)) {
					badDate = date;
					Assert.fail();
				}
			}
			return true;
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Detected date falls outside of given range." 
					+ "\nStarting Date: " + setDate 
					+ "\nEnding Date: " + today
					+ "\nDiscoverd Date: " + badDate
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\n------------------------------------------");
			Assert.fail("Detected date falls outside of given range, Exception: " + e);
			return false;
		}
	}
	
	protected boolean assertDatesMatch(List<LocalDate> allDates, String theDate) throws Exception, ComparisonFailure {
//		DateFormat originalFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
//		DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
//		Date ogSetDate = originalFormat.parse(theDate);
		LocalDate setDate = LocalDate.parse(theDate);
		LocalDate badDate = null;
		try { 
			for (LocalDate date: allDates) {
				if (!date.equals(setDate)) {
					badDate = date;
					Assert.fail();
				}
			}
			return true;
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Detected date does not match expected date." 
					+ "\nExpected Date: " + setDate 
					+ "\nDiscoverd Date: " + badDate
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\n------------------------------------------");
			Assert.fail("Detected date does not match expected date, Exception: " + e);
			return false;
		}
	}
	
	protected boolean assertDatesBefore(List<LocalDate> allDates, String theDate) throws Exception, ComparisonFailure {
//		DateFormat originalFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
//		DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
//		Date ogSetDate = originalFormat.parse(theDate);
		LocalDate setDate = LocalDate.parse(theDate);
		LocalDate badDate = null;
		try { 
			for (LocalDate date: allDates) {
				if (!date.isBefore(setDate)) {
					badDate = date;
					Assert.fail();
				}
			}
			return true;
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Detected date does not match expected date." 
					+ "\nExpected Date: " + setDate 
					+ "\nDiscoverd Date: " + badDate
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\n------------------------------------------");
			Assert.fail("Detected date does not match expected date, Exception: " + e);
			return false;
		}
	}
	
	protected boolean assertDatesAfter(List<LocalDate> allDates, String theDate) throws Exception, ComparisonFailure {
//		DateFormat originalFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
//		DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
//		Date ogSetDate = originalFormat.parse(theDate);
		LocalDate setDate = LocalDate.parse(theDate);
		LocalDate badDate = null;
		try { 
			for (LocalDate date: allDates) {
				if (!date.isAfter(setDate)) {
					badDate = date;
					Assert.fail();
				}
			}
			return true;
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Detected date does not match expected date." 
					+ "\nExpected Date: " + setDate 
					+ "\nDiscoverd Date: " + badDate
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\n------------------------------------------");
			Assert.fail("Detected date does not match expected date, Exception: " + e);
			return false;
		}
	}
	
	protected boolean assertListItemsMatchValue(String value, List<String> results) throws Exception, ComparisonFailure {
		String badResult = null;
		try { 
			for (String result : results) {
				if (!result.contains(value.replaceAll("[\\W+]", ""))) {
					badResult = result;
					Assert.fail();
				}
			}
			return true;
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Detected result does not match expected result." 
					+ "\nExpected Result: " + value 
					+ "\nDiscoverd result: " + badResult
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\n------------------------------------------");
			Assert.fail("Detected result does not match expected result, Exception: " + e);
			return false;
		}
	}
	
	protected boolean assertCorrectListSize(List<?> list, int size) throws Exception, ComparisonFailure {
		try { 
			Assert.assertTrue(list.size() == size);
			return true;
		} catch (AssertionError e) {
			int diff = list.size() - size;
			System.out.println(FAIL + "\n------------------------------------------");
			if (diff > 0) System.out.println("Reason: List is larger than expected." );
			if (diff < 0) System.out.println("Reason: List is smaller than expected." );
			System.out.println("Exception: " + (e.getClass()).toString().replace("class ", "") 
					+ "\n------------------------------------------");
			System.out.println("Size expected: " + size + "\nSize discovered: " + list.size());
			Assert.fail("Expected text not found in WebElement, Exception: " + e);
			return false;
		}
	}
	
	protected boolean assertTextDoesNotMatch(String expectedText, WebElement element) throws Exception, ComparisonFailure {
		waitUntilElementIsVisible(element);
		try { 
			Assert.assertEquals(expectedText.toLowerCase().replaceAll("[^a-z0-9]|\\s", ""), element.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""));
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Matching text found in element. Text should NOT match." 
					+ "\nExpected text: \"" + expectedText + "\"; Converted to \"" + expectedText.toLowerCase().replaceAll("[^a-z0-9]|\\s", "") + "\""
					+ "\nDiscovered text: \"" + element.getText() + "\"; Converted to \"" + element.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", "") + "\""
					+ "\n------------------------------------------");
			Assert.fail("Matching text found in element. Text should NOT match.");
			return false;
		} catch (AssertionError e) {	
			return true;
		}
	}

	protected boolean assertTextMatchesNoElement(String expectedText, String discoveredText) throws Exception {
		try {
			Assert.assertEquals(expectedText.toLowerCase().replaceAll("[^a-z0-9]|\\s", ""), discoveredText.toLowerCase().replaceAll("[^a-z0-9]|\\s", ""));
			return true;
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Expected text not found in element." 
					+ "\nExpected text: \"" + expectedText + "\"; Converted to \"" + expectedText.toLowerCase().replaceAll("[^a-z0-9]|\\s", "") + "\""
					+ "\nDiscovered text: \"" + discoveredText + "\"; Converted to \"" + discoveredText.toLowerCase().replaceAll("[^a-z0-9]|\\s", "") + "\""
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Expected text not found in element, Exception: " + e.getMessage());
			return false;
		}
	}
	
	protected void assertTextMatches(String currentPage, String[] totalPages, WebElement element) {
		try {
			assertThat(currentPage, isOneOf(totalPages[1], "10"));	
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------\""
					+ "\nReason: Last or tenth page of contract is NOT visible "
					+ "\nElement: <" + element.toString() + ">"
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Last or tenth page of contract is NOT visible, Exception: " + e.getMessage());

		}
	}
	
	protected void assertCheckboxSelected(WebElement element) {
		try {
			assertTrue(element.isSelected());
		} catch (AssertionError e) {
			System.out.println(FAIL 
					+ "\n------------------------------------------\""
					+ "\nReason: Checkbox is not selected"
					+ "\nElement: <" + element.toString() + ">"
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Checkbox is not selected. Exception: " + e.getMessage());
		}
	}
	
	protected boolean detectCheckboxSelected(WebElement element) {
		try {
			assertTrue(element.isSelected());
			return true;
		} catch (AssertionError e) {
			return false;
		}
	}

	public void isFileDownloaded(String fileName) throws Exception {
		File path = Constant.DOWNLOADS_DIRECTORY.toFile();
		File file = new File(Constant.DOWNLOADS_DIRECTORY.toFile() + File.separator + fileName);
		tempWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(120)).pollingEvery(Duration.ofMillis(100));
		tempWait.until(x -> file.exists());
		File[] matches = checkFileNameMatches(path, fileName);
		if (matches.length > 0) {
			while (matches.length > 0) {
				for (File match : matches) {
					match.delete();
					matches = checkFileNameMatches(path, fileName);
				}
			} 
		} else {
			System.out.println(FAIL + "\n------------------------------------------" + "\nReason: File not found."
					+ "\nFile Name: \"" + fileName + "\"");
			Assert.fail("Could not discover and delete files in Downloads folder.");
		}
	}
	
	private File[] checkFileNameMatches(File path, String fileName) throws Exception {
		File[] matches = path.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				if (name.contains(".pdf")) return name.contains(fileName.replace(".pdf", "").replaceAll("[0-9()]|\\s", ""));
				else return name.contains(fileName);
			}
		});
		return matches;	
	}

	protected void validateAttributeContainsValue(WebElement element, String tag, String value) throws Exception {
		try {
			wait.until(ExpectedConditions.attributeContains(element, tag, value));
		} catch (StaleElementReferenceException a) {
			WebElement newElement = element;
			validateAttributeContainsValue(newElement, tag, value);
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Unable to validate tag value in WebElement." 
					+ "\nElement Attribute: " + tag
					+ "\nExpected value: \"" + value + "\"" 
					+ "\nDiscovered value: \"" + element.getAttribute("value") + "\"" 
					+ "\nLocator: " + element.toString() 
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Unable to validate tag value in WebElement, Exception: " + e.getMessage());
		}
	}
	
	protected void validateAttributeEqualsValue(WebElement element, String tag, String value) throws Exception {
		try {
			wait.until(ExpectedConditions.attributeToBe(element, tag, value));
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Unable to validate tag value in WebElement." 
					+ "\nElement Attribute: " + tag
					+ "\nExpected value: \"" + value + "\"" 
					+ "\nDiscovered value: \"" + element.getAttribute("value") + "\"" 
					+ "\nLocator: " + element.toString() 
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Unable to validate tag value in WebElement, Exception: " + e.getMessage());
		}
	}

	protected boolean assertElementDoesNotExist(WebElement element) throws Exception {
		try {	
			Assert.assertFalse(element.isDisplayed());
		} catch (NoSuchElementException e) {
			return true;
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Element(s) should not exist at locator: " + element.toString() 
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Element(s) should not exist at locator: " + element.toString());
		}
		return false;
	}
	
	
	protected boolean assertElementsDoNotExist(List<WebElement> elements) throws Exception {
		try {
			assertTrue(elements.isEmpty());
			return true;
		} catch (AssertionError e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nReason: Element(s) should not exist at locator: " + elements.toString() 
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Element(s) should not exist at locator: " + elements.toString());
		}
		return false;
	}
	
	protected boolean checkIfElementIsDisabled(WebElement element) throws Exception {
		String checkDisabled = element.getAttribute("disabled");
		// If "disabled" attribute does not exist, validate false.
		// If "disabled" attribute exists but button is enabled, validate false.
		if (checkDisabled == null || (checkDisabled != null && checkDisabled.equals("true") == false)) {
			return false;
			// If "disabled" attribute exists and button is disabled, validate true.
		} else {
			return true;
		}
	}

	int sortAttempts = 0;
	
	protected void validateDropdownSelection(String text, Select element) throws Exception {
		try {
			Assert.assertEquals(text.replaceAll("[^a-z0-9]|\\s", ""), element.getFirstSelectedOption().getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""));
		} catch (AssertionError e) {
			
		}
	}
	
	/**********************************************************************************
	 ** JS SCROLL
	 **********************************************************************************/

	protected void scrollIntoView(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.visibilityOf(element)).isEnabled();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nUnable to scroll to the WebElement" 
					+ "\nElement: " + "<" + element.toString() + ">"
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Unable to scroll to the WebElement, Exception: " + e.getMessage());
		}
	}
	
	protected void scrollToElementByWebElementLocatorVertical(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.visibilityOf(element)).isEnabled();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0, -400)");
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nUnable to scroll to the WebElement" 
					+ "\nElement: " + "<" + element.toString() + ">"
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Unable to scroll to the WebElement, Exception: " + e.getMessage());
		}
	}
	
	protected void scrollToElementByWebElementLocatorHorizontal(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.visibilityOf(element)).isEnabled();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
			((JavascriptExecutor) driver).executeScript("window.scrollBy(-400, 0)");
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nUnable to scroll to the WebElement" 
					+ "\nElement: " + "<" + element.toString() + ">"
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Unable to scroll to the WebElement, Exception: " + e.getMessage());
		}
	}

	protected void scrollThroughElementBy(WebElement element, String num) {
		try {
			this.wait.until(ExpectedConditions.visibilityOf(element)).isEnabled();
			this.wait.until(ExpectedConditions.elementToBeClickable(element)).click();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollTo(0, " + num + ")", element);
		} catch (StaleElementReferenceException e) {
			System.out.print("_");
			scrollThroughElementBy(element, num);	
		} catch (Exception e) {
			System.out.println(FAIL + "\n------------------------------------------"
					+ "\nUnable to scroll to the WebElement" 
					+ "\nElement: " + "<" + element.toString() + ">"
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Unable to scroll through the WebElement, Exception: " + e.getMessage());
		}
	}
	
	public void waitAndclickElementUsingJS(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
			js.executeScript("arguments[0].click();", element);
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement staleElement = element;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(staleElement)).isEnabled();
			if (elementPresent == true) {
				js.executeScript("arguments[0].click();", elementPresent);
			}
		} catch (NoSuchElementException e) {
			System.out.println("Unable to JS click on the following WebElement: " + "<" + element.toString() + ">");
			Assert.fail("Unable to JS click on the WebElement, Exception: " + e.getMessage());
		}
	}
	
	public void jsClick(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);
	}

	/**********************************************************************************
	 ** WAIT METHODS
	 **********************************************************************************/

	public boolean waitForJSandJQueryToLoad() {

	    WebDriverWait wait = new WebDriverWait(driver, 30);

	    // wait for jQuery to load
	    ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
	      @Override
	      public Boolean apply(WebDriver driver) {
	        try {
	          return ((Long)((JavascriptExecutor)getDriver()).executeScript("return jQuery.active") == 0);
	        }
	        catch (Exception e) {
	          // no jQuery present
	          return true;
	        }
	      }
	    };

	    // wait for Javascript to load
	    ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
	      @Override
	      public Boolean apply(WebDriver driver) {
	        return ((JavascriptExecutor)getDriver()).executeScript("return document.readyState")
	        .toString().equals("complete");
	      }
	    };

	  return wait.until(jQueryLoad) && wait.until(jsLoad);
	}
	
	protected void waitForReadyState() {
		new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd -> ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
	}
	
	protected void waitUntilElementIsVisible(WebElement element) throws InterruptedException {
		waitForJSandJQueryToLoad();
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement newElement = element;
			waitUntilElementIsVisible(newElement);
		} catch (TimeoutException e) {
			System.out.println(FAIL 
					+ "\n------------------------------------------" 
					+ "\nWebElement is NOT visible "
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("WebElement is NOT visible, Exception: " + e.getMessage());
		}
	}

	protected boolean waitUntilElementIsClickable(WebElement element) {
		waitForJSandJQueryToLoad();
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element));
			return true;
		} catch (TimeoutException e) {
			System.out.println(FAIL 
					+ "\n------------------------------------------" 
					+ "\nWebElement is NOT clickable "
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("WebElement is NOT clickable, Exception: " + e.getMessage());	
			return false;
		}
	}
	
	public boolean waitUntilPreLoadElementDissapears(WebElement element) {
		return this.wait.until(ExpectedConditions.invisibilityOf(element));
	}
	
	public boolean waitUntilLocatedElementDissapears(By locator) {
		return this.wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	}
	
	public boolean waitUntilLocatedElementsDissapear(List<WebElement> elements) {
		return this.wait.until(ExpectedConditions.invisibilityOfAllElements(elements));
	}
	
	public boolean waitUntilTextAppears(WebElement element, String text) {
		tempWait = new WebDriverWait(driver, 10);
		return this.tempWait.until(ExpectedConditions.textToBePresentInElement(element, text));
	}
	
	public String waitUntilURLMatches(String urlToWaitFor) {
		try {
			driver.getCurrentUrl();
			this.wait.until(ExpectedConditions.urlMatches(urlToWaitFor));
			return urlToWaitFor;
		} catch (Exception e) {
			System.out.println(FAIL 
					+ "\n------------------------------------------" 
					+ "\nTimed out waiting for the URL: " + urlToWaitFor
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Timed out waiting for the URL, Exception: " + e.getMessage());	
			return e.getMessage();
		}
	}
	
	public String waitUntilURLContains(String urlToWaitFor) {
		try {
			driver.getCurrentUrl();
			this.wait.until(ExpectedConditions.urlContains(urlToWaitFor));
			return urlToWaitFor;
		} catch (Exception e) {
			System.out.println(FAIL 
					+ "\n------------------------------------------" 
					+ "\nTimed out waiting for the URL: " + urlToWaitFor
					+ "\nException: " + (e.getClass()).toString().replace("class ", "") 
					+ "\nMessage: " + e.getMessage()
					+ "\n------------------------------------------");
			Assert.fail("Timed out waiting for the URL, Exception: " + e.getMessage());				
			return e.getMessage();
		}
	}
	
	/**********************************************************************************
	 **ALERT & POPUPS METHODS
	 **********************************************************************************/
	public void closePopups(By locator) throws InterruptedException {
		try {
			List<WebElement> elements = this.wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
			for (WebElement element : elements) {
				if (element.isDisplayed()) {
					element.click();
					this.wait.until(ExpectedConditions.invisibilityOfAllElements(elements));
					System.out.println("The popup has been closed Successfully!");
				}
			}
		} catch (Exception e) {
			System.out.println("Exception! - could not close the popup!, Exception: " + e.toString());
			throw (e);
		}
	}

	public boolean checkPopupIsVisible() {
		try {
			@SuppressWarnings("unused")
			Alert alert = wait.until(ExpectedConditions.alertIsPresent());
			return true;
		} catch (Exception e) {
			System.err.println("Error came while waiting for the alert popup to appear. " + e.getMessage());
		}
		return false;
	}

	public boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void closeAlertPopupBox() throws AWTException, InterruptedException {
		try {
			Alert alert = this.wait.until(ExpectedConditions.alertIsPresent());
			alert.accept();
		} catch (UnhandledAlertException f) {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			System.out.println("Unable to close the popup");
			Assert.fail("Unable to close the popup, Exception: " + e.getMessage());
		}
	}
	
	public void closeAlertPopupBoxDismiss() throws AWTException, InterruptedException {
		try {
			Alert alert = this.wait.until(ExpectedConditions.alertIsPresent());
			alert.dismiss();
		} catch (UnhandledAlertException f) {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			System.out.println("Unable to close the popup");
			Assert.fail("Unable to close the popup, Exception: " + e.getMessage());
		}
	}

	/*****************************************************************
	 ** EXTENT REPORT
	 *****************************************************************/

	private static String returnDateStamp(String fileExtension) {
		Date d = new Date();
		String date = d.toString().replace(":", "_").replace(" ", "_") + fileExtension;
		return date;
	}

	public static void captureScreenshot() throws IOException {
		File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		screenshotName = returnDateStamp(".png");
		FileUtils.copyFile(srcFile, new File(System.getProperty("user.dir") + "/output/imgs/" + screenshotName));
		Reporter.addStepLog("Taking a screenshot\n");
		Reporter.addStepLog("<br>");
		Reporter.addStepLog("<a target=\"_blank\" href=" + returnScreenshotName() + "><img src="
				+ returnScreenshotName() + " height=200 width=300></img></a>");
	}

	private static String returnScreenshotName() {
		return (System.getProperty("user.dir") + "/output/imgs/" + screenshotName).toString();
	}

	private static void copyFileUsingStream(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;

		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} finally {
			is.close();
			os.close();
		}
	}

	public static void copyLatestExtentReport() throws IOException {
		Date d = new Date();
		String date = d.toString().replace(":", "_").replace(" ", "_");
		File source = new File(System.getProperty("user.dir") + "/output/report.html");
		File dest = new File(System.getProperty("user.dir") + "/output/" + date.toString() + ".html");
		copyFileUsingStream(source, dest);

	}
}
