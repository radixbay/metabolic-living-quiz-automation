package pageObjects.renewal;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class RenewalVideo extends BasePage {

	public RenewalVideo() throws IOException {
		super();
	}

	private @FindBy(xpath = "//h1[@id='video-h1']") WebElement text_StartingHeadline;
	private @FindBy(xpath = "//h2[@id='video-h2']") WebElement text_StartingSubheadline;
	private @FindBy(xpath = "//div[@class='embed-container']") WebElement container_Video;
	private @FindBy(xpath = "//iframe[@id='ytplayer']") WebElement iframe_Video;
	private @FindBy(xpath = "//div[@class='testimonials-fb']/div[1]") WebElement text_Testimonial1;
	private @FindBy(xpath = "//div[@class='testimonials-fb']/div[2]") WebElement text_Testimonial2;
	private @FindBy(xpath = "//div[@class='testimonials-fb']//p[@class='testimonial-disclaimer']") WebElement text_TestimonialDisclaimer;
	private @FindBy(xpath = "//div[@class='intro']//h3") WebElement text_FAQ;
	private @FindBy(xpath = "//div[@class='intro']//div[1]//h4[1]//span[1]") WebElement text_Q1;
	private @FindBy(xpath = "//div[@class='intro']//div[2]//h4[1]//span[1]") WebElement text_Q2;
	private @FindBy(xpath = "//div[@class='intro']//div[3]//h4[1]//span[1]") WebElement text_Q3;
	private @FindBy(xpath = "//div[@class='intro']//div[4]//h4[1]//span[1]") WebElement text_Q4;
	private @FindBy(xpath = "//div[@class='intro']//section[@class='faqs']//div[1]//div[1]") WebElement text_Q1Answer;
	private @FindBy(xpath = "//div[@class='intro']//section[@class='faqs']//div[2]//div[1]") WebElement text_Q2Answer;
	private @FindBy(xpath = "//div[@class='intro']//section[@class='faqs']//div[3]//div[1]") WebElement text_Q3Answer;
	private @FindBy(xpath = "//div[@class='intro']//section[@class='faqs']//div[4]//div[1]") WebElement text_Q4Answer;
	private @FindBy(xpath = "//input[@alt='Submit']") WebElement button_AddToCart;
	
	private @FindBy(xpath = "//div[@class='ytp-progress-bar']") WebElement ytplayer_ProgressBar;

	
	
	
	// **************************
	// Clicks
	// **************************	
	public RenewalVideo scrollVideo() throws Exception {
		scrollIntoView(text_StartingHeadline);	
		waitAndClickElement(container_Video);
		driver.switchTo().frame("ytplayer");		
		waitAndClickElement(ytplayer_ProgressBar);
		driver.switchTo().defaultContent();
		waitAndClickElement(container_Video);
		return new RenewalVideo();
	}
	
	public RenewalVideo clickAddToCart() throws Exception {
		waitAndClickElement(button_AddToCart);
		return new RenewalVideo();
	}
	
	
	// **************************
	// Validation
	// **************************	
	public RenewalVideo validatePage() throws Exception {
		waitUntilURLContains("video?");
		
		waitUntilElementIsVisible(text_StartingHeadline);
		waitUntilElementIsVisible(text_StartingSubheadline);
		waitUntilElementIsVisible(iframe_Video);
		waitUntilElementIsVisible(text_Testimonial1);
		waitUntilElementIsVisible(text_Testimonial2);
		waitUntilElementIsVisible(text_TestimonialDisclaimer);
		waitUntilElementIsVisible(text_FAQ);
		
		for (int i = 0; i < 4; i++) {
			waitAndClickElement(driver.findElement(By.xpath("//div[@class='intro']//div[" +Integer.toString(i+1)+ "]//h4[1]//span[1]")));
		}

		waitUntilElementIsVisible(text_Q1);
		waitUntilElementIsVisible(text_Q2);
		waitUntilElementIsVisible(text_Q3);
		waitUntilElementIsVisible(text_Q4);

		return new RenewalVideo();
	}
}
