package pageObjects.renewal;

import java.io.IOException;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;
import pageObjects.factor.FactorSignUp;
import utils.ScoringTable;

public class RenewalSignUp extends BasePage {

	public RenewalSignUp() throws IOException {
		super();
	}
	
	public String pageTypeResult;
	public String[] strArray;
	public int pageTypeResultNum;
	
	private @FindBy(xpath = "//img[@class='logo']") WebElement logo;
	private @FindBy(xpath = "//h1[@id='page-title']") WebElement text_QuizResults;
	private @FindBy(xpath = "//p[@class='important']") WebElement text_Subheader;
	private @FindBy(xpath = "//input[@id='form-email']") WebElement input_Email;
	private @FindBy(xpath = "//input[@id='form-phone']") WebElement input_Phone;
	private @FindBy(xpath = "//button[@type='submit']") WebElement button_Submit;
	private @FindBy(xpath = "//input[@id='customizedplan']") WebElement checkbox_PrivacyTerms;
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[1]/a") WebElement link_AboutDrJade;
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[2]/a") WebElement link_PrivacyPolicy;
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[3]/a") WebElement link_TermsConditions;
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[4]/a") WebElement link_TermsOfSale;
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[5]/a") WebElement link_ContactUs;
	private @FindBy(xpath = "//p[@class='ftr-info']") WebElement text_Info;
	private @FindBy(xpath = "//section[@class='disclaimer-txt']//p") WebElement text_Disclaimer;

	// **************************
	// Clicks
	// **************************	
	public FactorSignUp clickSubmit() throws Exception {
		waitAndClickElement(button_Submit);
		return new FactorSignUp();
	}	
	
	// **************************
	// Key Sending
	// **************************	
	public String inputEmail() throws Exception {
		String input = RandomStringUtils.randomAlphabetic(5) + RandomStringUtils.randomNumeric(5) + "@qopow.com";
		sendKeysToWebElement(input_Email, input);
		return input;
	}	

	// **************************
	// Validation
	// **************************		
	public RenewalSignUp validateSignUpPage() throws Exception {
		waitUntilURLContains("https://www.metabolicrenewal.com/p/mb/signup?");
		waitUntilElementIsVisible(logo);
		waitUntilElementIsVisible(text_QuizResults);
		waitUntilElementIsVisible(text_Subheader);
		waitUntilElementIsVisible(input_Email);
		waitUntilElementIsVisible(input_Phone);
		waitUntilElementIsVisible(button_Submit);
		waitUntilElementIsVisible(checkbox_PrivacyTerms);
		waitUntilElementIsVisible(link_AboutDrJade);
		waitUntilElementIsVisible(link_PrivacyPolicy);
		waitUntilElementIsVisible(link_TermsConditions);
		waitUntilElementIsVisible(link_TermsOfSale);
		waitUntilElementIsVisible(link_ContactUs);
		waitUntilElementIsVisible(text_Info);
		waitUntilElementIsVisible(text_Disclaimer);
		
		waitUntilElementIsClickable(input_Email);
		waitUntilElementIsClickable(input_Phone);
		waitUntilElementIsClickable(button_Submit);
		waitUntilElementIsClickable(checkbox_PrivacyTerms);
		return new RenewalSignUp();
	}
	
	public RenewalSignUp validateHormoneTypePageResult() throws Exception {
		System.out.println("\nExpected:\t Hormone Type #" + (ScoringTable.typeResult));
		pageTypeResult = text_QuizResults.getText().replaceAll("\\r\\n|\\r|\\n", "").replace("#", "");
		strArray = pageTypeResult.split(" ");
		pageTypeResultNum = Integer.parseInt(strArray[3]);
		System.out.println("Discovered:\t Hormone Type #" + Integer.toString(pageTypeResultNum));
		assertNumbersMatch(pageTypeResultNum, ScoringTable.typeResult);
		return new RenewalSignUp();
	}
}
