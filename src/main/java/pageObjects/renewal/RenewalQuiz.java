package pageObjects.renewal;

import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.BasePage;
import utils.ScoringTable;

public class RenewalQuiz extends BasePage {

	public RenewalQuiz() throws IOException {
		super();
	}

	public WebElement questionPath;
	public WebElement answerPath;
	public String question;
	public String answer = "";
	public int questionNumber;
	public int aNum;
	public int upperbound;
	public int q2Answer;
	public int userType;

	// Quiz Elements
	private @FindBy(xpath = "//img[@class='logo']") WebElement img_Title;
	private @FindBy(xpath = "//h1[@id='page-title']") WebElement text_Subhead;

	// Footer Elements
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[1]/a") WebElement link_AboutDrJade;
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[2]/a") WebElement link_PrivacyPolicy;
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[3]/a") WebElement link_TermsConditions;
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[4]/a") WebElement link_TermsOfSale;
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[5]/a") WebElement link_ContactUs;
	private @FindBy(xpath = "//p[@class='ftr-info']") WebElement text_Info;
	private @FindBy(xpath = "//section[@class='disclaimer-txt']//p") WebElement text_Disclaimer;
	
	// Calculation
	private @FindBy(xpath = "//h2[@class='transition-hide']") WebElement text_Calculation1;
	private @FindBy(xpath = "//h2[@class='transition-show']") WebElement text_Calculation2;

	public RenewalQuiz navigateToMetabolicRenewal() throws Exception {
		getDriver().get("https://www.metabolicrenewal.com/p/mb/quiz");
		return new RenewalQuiz();
	}

	// **************************
	// Answer Selection
	// **************************
	public int setAnswer() throws Exception {
		switch (questionNumber) {
		case 1:
		case 3:
		case 5:
		case 12:
			upperbound = 5;
			break;
		case 2:
			upperbound = 9;
			break;
		case 4:
			upperbound = 7;
			break;
		case 11:
		case 13:
			upperbound = 6;
			break;
		case 14:
			upperbound = 2;
			break;
		default:
			break;
		}

		aNum = randomizeAnswer(upperbound);
		return aNum;
	}

	public int randomizeAnswer(int upperbound) throws Exception {
		Random range = new Random();
		int int_random = range.nextInt(upperbound);
		return int_random + 1;
	}

	// **************************
	// Extract
	// **************************
	public String getQuestion(int qNum) throws Exception {
		questionNumber = qNum;
		questionPath = driver.findElement(By.xpath(
				"//div[@class='quiz']//div[@id='quiz-container']/div[@class='metabolicrenewal-question-container']["
						+ Integer.toString(qNum) + "]/h2"));
		question = questionPath.getText().replaceAll("\\r\\n|\\r|\\n", " ");
		return question;
	}

	// **************************
	// Clicks
	// **************************

	public String getAnswer(int qNum) throws Exception {
		setAnswer();
		answerPath = driver.findElement(By.xpath(
				"//div[@class='quiz']//div[@id='quiz-container']/div[@class='metabolicrenewal-question-container']["
						+ Integer.toString(qNum) + "]/div[@class='response-row'][" + Integer.toString(aNum) + "]"));
		answer = answerPath.getText().replaceFirst("\\r\\n|\\r|\\n", " - ").replaceAll("\\r\\n|\\r|\\n", " ");
		waitAndClickElement(answerPath);
		ScoringTable.getAnswerId(aNum);
		ScoringTable.increaseTotal(qNum);
		if (ScoringTable.typeResult != 0) {
			userType = ScoringTable.typeResult;
		}
		return answer;
	}

	public RenewalQuiz validateQuizPage() throws Exception {
		waitUntilURLMatches("https://www.metabolicrenewal.com/p/mb/quiz");
		waitUntilElementIsVisible(img_Title);
		waitUntilElementIsVisible(text_Subhead);

		waitUntilElementIsVisible(link_AboutDrJade);
		waitUntilElementIsVisible(link_PrivacyPolicy);
		waitUntilElementIsVisible(link_TermsConditions);
		waitUntilElementIsVisible(link_ContactUs);
		waitUntilElementIsVisible(text_Info);
		waitUntilElementIsVisible(text_Disclaimer);

		waitUntilElementIsClickable(link_AboutDrJade);
		waitUntilElementIsClickable(link_PrivacyPolicy);
		waitUntilElementIsClickable(link_TermsConditions);
		waitUntilElementIsClickable(link_ContactUs);

		return new RenewalQuiz();
	}
	
	int a = 0;
	public void validateCheck(WebElement element) throws Exception {
		try {
			tempWait = new WebDriverWait(driver, 0);
			tempWait.until(ExpectedConditions.attributeContains(element, "display", "none"));
			System.out.println(" --> [PASSED]");
		} catch (TimeoutException e) {
			if (a == 1000) throw e;	
			a++;	
			validateCheck(element);
		}
	}
	
	public RenewalQuiz validateCalculation() throws Exception {
		System.out.print("\t\"Calculating Your Hormone Type and Customizing Your Plan...\"");
		validateCheck(text_Calculation2);
		System.out.print("\t\"Did you know? We have helped over 7,774,000 women discover their hormone type for losing weight.\"");
		validateCheck(text_Calculation1);
		System.out.println("");
		return new RenewalQuiz();
	}


}
