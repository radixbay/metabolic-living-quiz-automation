package pageObjects.factor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.BasePage;

public class FactorQuiz extends BasePage {

	public FactorQuiz() throws IOException {
		super();
	}

	public WebElement questionPath;
	public WebElement answerPath;
	public String question;
	public String answer = "";
	public int questionNumber;
	public int answerNumber;
	public int upperbound;
	List<Integer> multiSelectSet = new ArrayList<Integer>();

	// Quiz Elements
	private @FindBy(xpath = "//h1[@class='initial-heading starting-headline']") WebElement text_Title;
	private @FindBy(xpath = "//h2[@class='initial-heading starting-subheadline']") WebElement text_Subhead;
	private @FindBy(xpath = "//div[@class='container']//form//h2") WebElement text_Q;
	private @FindBy(xpath = "//a[@class='answer-button']") WebElement button_Submit;

	// Footer Elements
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[1]/a") WebElement link_AboutUs;
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[2]/a") WebElement link_PrivacyPolicy;
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[3]/a") WebElement link_TermsConditions;
	private @FindBy(xpath = "//ul[@class='ftr-menu']/li[4]/a") WebElement link_ContactUs;
	private @FindBy(xpath = "//p[@class='ftr-info']") WebElement text_Info;
	private @FindBy(xpath = "//div[@class='ftr-disclaimer']//p") WebElement text_Disclaimer;
	
	// Calculation Elements
	private @FindBy(xpath = "//li[@class='check-one']") WebElement li_check1;
	private @FindBy(xpath = "//li[@class='check-two']") WebElement li_check2;
	private @FindBy(xpath = "//li[@class='check-three']") WebElement li_check3;

	
	public FactorQuiz navigateToMetabolicFactor() throws Exception {
		getDriver().get("https://www.metabolicfactor.com/p/mb/quiz");
		return new FactorQuiz();
	}

	// **************************
	// Answer Selection
	// **************************
	public int setAnswer() throws Exception {
		switch (questionNumber) {
		case 1:
		case 2:
			upperbound = 6;
			break;
		case 3:
			upperbound = 4;
			break;
		case 4:
		case 5:
			upperbound = 2;
			break;
		case 6:
			upperbound = 8;

			break;
		case 7:
			upperbound = 3;
			break;
		case 8:
			upperbound = 10;
			break;
		default:
			break;
		}

		answerNumber = randomizeAnswer(upperbound);
		return answerNumber;
	}

	public int randomizeAnswer(int upperbound) throws Exception {
		Random range = new Random();
		int int_random = range.nextInt(upperbound);
		return int_random + 1;
	}

	// **************************
	// Extract
	// **************************
	public String getQuestion(int qNum) throws Exception {
		questionNumber = qNum;
		questionPath = driver
				.findElement(By.xpath("//div[@id='question-container']//div[" + Integer.toString(qNum) + "]//h2[1]"));
		question = questionPath.getText().replaceAll("\\r\\n|\\r|\\n", " ");
		return question;
	}

	// **************************
	// Clicks
	// **************************

	public String getAnswer(int qNum) throws Exception {
		List<String> answerList = new ArrayList<String>();
		Random rand = new Random();
		int range;
		switch (qNum) {
		case 6:
			range = rand.nextInt(qNum - 1);
			for (int i = 0; i < range + 2; i++) {
				setAnswer();
				if (multiSelectSet.contains(answerNumber)) continue;	
				multiSelectSet.add(answerNumber);
				answerPath = driver.findElement(By.xpath("//div[@id='question-container']//div["
						+ Integer.toString(qNum) + "]//div[" + Integer.toString(this.answerNumber) + "]//div[1]"));
				answerList.add(answerPath.getText().replaceAll("\\r\\n|\\r|\\n", " --- "));
				waitAndClickElement(answerPath);	
			}
			System.out.println("Answer:");
			for (int i = 0; i < answerList.size(); i++) System.out.println("\t- " + answerList.get(i));
			System.out.println("");
			multiSelectSet.clear();
			break;
		case 8:
			range = rand.nextInt(qNum);
			for (int i = 0; i < range + 2; i++) {
				setAnswer();
				if (multiSelectSet.contains(answerNumber)) continue;
				multiSelectSet.add(answerNumber);
				answerPath = driver.findElement(By.xpath("//div[@id='question-container']//div["
						+ Integer.toString(qNum) + "]//div[" + Integer.toString(this.answerNumber) + "]//div[1]"));
				answerList.add(answerPath.getText().replaceAll("\\r\\n|\\r|\\n", " --- "));
				waitAndClickElement(answerPath);		
			}
			System.out.println("Answer:");
			for (int i = 0; i < answerList.size(); i++) System.out.println("\t- " + answerList.get(i));
			System.out.println("");
			multiSelectSet.clear();
			break;	
		default:
			setAnswer();
			answerPath = driver.findElement(By.xpath("//div[@id='question-container']//div[" + Integer.toString(qNum)
					+ "]//div[" + Integer.toString(this.answerNumber) + "]//div[1]"));
			switch (qNum) {
			case 2:
				answer = answerPath.getText().replace("51 +", "51+");
			case 5:
				answer = answerPath.getText().replaceAll("\\r\\n|\\r|\\n", ": ").replace("(", "").replace(")", "");
				break;
			case 7:
				answer = answerPath.getText().replaceAll("\\r\\n|\\r|\\n", " ");
				break;
			default: 
				answer = answerPath.getText().replaceAll("\\r\\n|\\r|\\n", " --- ");
				break;
			}
			waitAndClickElement(answerPath);
			return answer;
		}	
		return null;
	}

	public FactorQuiz clickSubmit(int i) throws Exception {
		waitAndClickElement(driver.findElement(By.xpath("(//a[@class='answer-button'])[" + Integer.toString(i) + "]")));
		return new FactorQuiz();
	}

	// **************************
	// Validation
	// **************************

	public FactorQuiz validateQuizPage() throws Exception {
		waitUntilURLMatches("https://www.metabolicfactor.com/p/mb/quiz");
		waitUntilElementIsVisible(text_Title);
		waitUntilElementIsVisible(text_Subhead);
		waitUntilElementIsVisible(text_Q);

		waitUntilElementIsVisible(link_AboutUs);
		waitUntilElementIsVisible(link_PrivacyPolicy);
		waitUntilElementIsVisible(link_TermsConditions);
		waitUntilElementIsVisible(link_ContactUs);
		waitUntilElementIsVisible(text_Info);
		waitUntilElementIsVisible(text_Disclaimer);

		waitUntilElementIsClickable(link_AboutUs);
		waitUntilElementIsClickable(link_PrivacyPolicy);
		waitUntilElementIsClickable(link_TermsConditions);
		waitUntilElementIsClickable(link_ContactUs);

		return new FactorQuiz();
	}
	
	public FactorQuiz validateCalculation() throws Exception {
		System.out.print("\t\"Calculating Your Fat Burning Type\" ");
		validateCheck(li_check1);
		System.out.print("\t\"Customizing Your Weight Loss Plan\" ");
		validateCheck(li_check2);
		System.out.print("\t\"Preparing Your Results\" ");
		validateCheck(li_check3);
		return new FactorQuiz();
	}
	
	int a = 0;
	public void validateCheck(WebElement element) throws Exception {
		try {
			tempWait = new WebDriverWait(driver, 0);
			tempWait.until(ExpectedConditions.attributeContains(element, "display", "list-item"));
			System.out.println(" --> [PASSED]");
		} catch (TimeoutException e) {
			if (a == 1000) throw e;	
			a++;	
			validateCheck(element);
		}
	}
}
