package pageObjects.factor;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class FactorVideo extends BasePage {

	public FactorVideo() throws IOException {
		super();
	}

	private @FindBy(xpath = "//h1[@class='starting-headline']") WebElement text_StartingHeadline;
	private @FindBy(xpath = "//h2[@class='starting-subheadline']") WebElement text_StartingSubheadline;
	private @FindBy(xpath = "//div[@class='embed-container']") WebElement container_Video;
	private @FindBy(xpath = "//iframe[@id='ytplayer']") WebElement iframe_Video;
	private @FindBy(xpath = "//div[@class='testimonials-fb']/div[1]") WebElement text_Testimonial1;
	private @FindBy(xpath = "//div[@class='testimonials-fb']/div[2]") WebElement text_Testimonial2;
	private @FindBy(xpath = "//div[@class='testimonials-fb']//p[@class='testimonial-disclaimer']") WebElement text_TestimonialDisclaimer;
	private @FindBy(xpath = "//section[@class='faqs']//h3") WebElement text_FAQ;
	private @FindBy(xpath = "//div[@class='cta-before']//div[1]//h4[1]//span[1]") WebElement text_Q1;
	private @FindBy(xpath = "//div[@class='cta-before']//div[2]//h4[1]//span[1]") WebElement text_Q2;
	private @FindBy(xpath = "//div[@class='cta-before']//div[3]//h4[1]//span[1]") WebElement text_Q3;
	private @FindBy(xpath = "//div[@class='cta-before']//div[4]//h4[1]//span[1]") WebElement text_Q4;
	private @FindBy(xpath = "//div[@class='cta-before']//div[5]//h4[1]//span[1]") WebElement text_Q5;
	private @FindBy(xpath = "//div[@class='cta-before']//div[6]//h4[1]//span[1]") WebElement text_Q6;
	private @FindBy(xpath = "//section[@class='faqs']//div[1]//div[1]") WebElement text_Q1Answer;
	private @FindBy(xpath = "//section[@class='faqs']//div[2]//div[1]") WebElement text_Q2Answer;
	private @FindBy(xpath = "//section[@class='faqs']//div[3]//div[1]") WebElement text_Q3Answer;
	private @FindBy(xpath = "//section[@class='faqs']//div[4]//div[1]") WebElement text_Q4Answer;
	private @FindBy(xpath = "//section[@class='faqs']//div[5]//div[1]") WebElement text_Q5Answer;
	private @FindBy(xpath = "//section[@class='faqs']//div[6]//div[1]") WebElement text_Q6Answer;
	private @FindBy(xpath = "//img[@alt='Submit Button']") WebElement button_AddToCart;	
	private @FindBy(xpath = "//div[@class='ytp-progress-bar']") WebElement ytplayer_ProgressBar;
	private @FindBy(xpath = "//div[@class='ytp-play-button ytp-button'") WebElement ytplayer_PlayButton;
	private @FindBy(xpath = "//div[@class='cta-countdown']") WebElement div_Countdown;
	private @FindBy(xpath = "//label[@for='option-1']") WebElement button_OnlineVersion;

	

	// **************************
	// Clicks
	// **************************	
	public FactorVideo scrollVideo() throws Exception {
		scrollIntoView(text_StartingHeadline);	
		driver.switchTo().frame("ytplayer");		
		waitAndClickElement(ytplayer_ProgressBar);
		driver.switchTo().defaultContent();
		return new FactorVideo();
	}
	
	public FactorVideo selectVersion() throws Exception {
		waitAndClickElement(button_OnlineVersion);
		return new FactorVideo();
	}
	
	public FactorVideo clickAddToCart() throws Exception {
		waitAndClickElement(button_AddToCart);
		return new FactorVideo();
	}
	
	
	
	
	// **************************
	// Validation
	// **************************	
	public FactorVideo validatePage() throws Exception {
		waitUntilURLContains("video?");
		
		waitUntilElementIsVisible(text_StartingHeadline);
		waitUntilElementIsVisible(text_StartingSubheadline);
		waitUntilElementIsVisible(iframe_Video);
		waitUntilElementIsVisible(text_Testimonial1);
		waitUntilElementIsVisible(text_Testimonial2);
		waitUntilElementIsVisible(text_TestimonialDisclaimer);
		waitUntilElementIsVisible(text_FAQ);
		
		for (int i = 0; i < 6; i++) {
			waitAndClickElement(driver.findElement(By.xpath("//div[@class='cta-before']//div[" +Integer.toString(i+1)+ "]//h4[1]//span[1]")));
		}

		waitUntilElementIsVisible(text_Q1);
		waitUntilElementIsVisible(text_Q2);
		waitUntilElementIsVisible(text_Q3);
		waitUntilElementIsVisible(text_Q4);
		waitUntilElementIsVisible(text_Q5);
		waitUntilElementIsVisible(text_Q6);
		return new FactorVideo();
	}
	
	public FactorVideo validateCountdown() throws Exception {
		waitUntilElementIsVisible(div_Countdown);
		Thread.sleep(3000);
		return new FactorVideo();
	}

	
	
}
