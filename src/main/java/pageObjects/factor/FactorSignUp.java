package pageObjects.factor;

import java.io.IOException;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class FactorSignUp extends BasePage {

	public FactorSignUp() throws IOException {
		super();
	}

	private @FindBy(xpath = "//h1[@class='quiz-results']") WebElement text_QuizResults;
	private @FindBy(xpath = "//span[@class='results-text']") WebElement text_YourResults;
	private @FindBy(xpath = "//span[@class='burning-level']") WebElement text_BurningLevel;
	private @FindBy(xpath = "//p[@class='subhead results-subhead']") WebElement text_ResultsSubhead;
	private @FindBy(xpath = "//input[@id='form-email']") WebElement input_Email;
	private @FindBy(xpath = "//input[@class='email-submit']") WebElement button_Submit;
	private @FindBy(xpath = "//span[@class='checkmark']") WebElement checkbox_Terms;
	
	// **************************
	// Clicks
	// **************************	
	public FactorSignUp clickSubmit() throws Exception {
		waitAndClickElement(button_Submit);
		return new FactorSignUp();
	}
	
	// **************************
	// Key Sending
	// **************************	
	
	public String inputEmail() throws Exception {
		String input = RandomStringUtils.randomAlphabetic(5) + RandomStringUtils.randomNumeric(5) + "@qopow.com";
		sendKeysToWebElement(input_Email, input);
		return input;
	}
	
	// **************************
	// Validation
	// **************************	
	
	public FactorSignUp validatePage() throws Exception {
		waitUntilURLContains("signup?");
		
		waitUntilElementIsVisible(text_QuizResults);
		waitUntilElementIsVisible(text_YourResults);
		waitUntilElementIsVisible(text_BurningLevel);
		waitUntilElementIsVisible(text_ResultsSubhead);
		waitUntilElementIsVisible(input_Email);
		waitUntilElementIsVisible(button_Submit);
		waitUntilElementIsVisible(checkbox_Terms);

		waitUntilElementIsClickable(input_Email);
		waitUntilElementIsClickable(button_Submit);
		waitUntilElementIsClickable(checkbox_Terms);
		return new FactorSignUp();
	}

}
