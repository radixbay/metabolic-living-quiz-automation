package pageObjects.factor;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class FactorPayment extends BasePage {

	public FactorPayment() throws IOException {
		super();
	}

	private @FindBy(xpath = "//label[@class='payment_method paypal-payment-method']//span") WebElement label_PayPal;
	private @FindBy(xpath = "//input[@value='paypal']") WebElement radioButton_PayPal;

	
	// **************************
	// Clicks
	// **************************
	public FactorPayment clickPayPal() throws Exception {
		waitUntilElementIsVisible(label_PayPal);
		scrollIntoView(label_PayPal);
		waitAndclickElementUsingJS(label_PayPal);
		return new FactorPayment();
	}
	
	// **************************
	// Validation
	// **************************
	public FactorPayment validatePayPalSelected() throws Exception {
		assertCheckboxSelected(radioButton_PayPal);
		return new FactorPayment();
	}

}
